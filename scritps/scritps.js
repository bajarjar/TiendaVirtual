var appAngular = angular.module("Tienda", ['ngRoute']);
appAngular.config(function ($routeProvider) {
    $routeProvider
            .when('/home', {
                templateUrl: 'home.html',
                controller: 'TiendaControler',

            })
            .when('/cart', {
                templateUrl: 'cart.html',
                controller: 'CarroController'
            })
//                    .when('/contacto', {
//                    templateUrl: 'pages/contacto.html',
//                            controller: 'contactController'
//                    })
            .otherwise({
                redirectTo: '/home'
            });
});


appAngular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined)
                    return ''
                var onlyNumeric = inputValue.replace(/[^0-9]/g, '');
                if (onlyNumeric != inputValue) {
                    modelCtrl.$setViewValue(onlyNumeric);
                    modelCtrl.$render();
                }
                return onlyNumeric;
            });
        }
    };
});


appAngular.service('serviceDatos', [function ()
    {


        this.carroCompras = [];
        this.cantidadItem = 0;
        this.textoAlerta = 0;

        if (localStorage.getItem("products")) {
            this.products = JSON.parse(localStorage.getItem("products"));
        } else {
            this.products = products.products;
        }

        if (localStorage.getItem("vcarroComprasDef")) {
            this.vcarroComprasDef = JSON.parse(localStorage.getItem("vcarroComprasDef"));
        } else {
            this.vcarroComprasDef = [];
        }



    }]);


appAngular.service('serviceFunciones', [function ()
    {
        this.fnGetCarroCompraDef = function (carroCompras) {
            var carroComprasDef1 = [];

            for (var propertyName in carroCompras) {
                carroComprasDef1.push(carroCompras[propertyName]);
            }

            return carroComprasDef1;

        };

        this.fnGetCarroCompra = function (carroComprasDef) {

            var carroCompras1 = [];
            angular.forEach(carroComprasDef, function (value, key) {
                carroCompras1[value.id] = value;
            });
            return carroCompras1;

        };


    }]);

appAngular.controller('TiendaControler', ['$scope', 'serviceDatos', 'serviceFunciones', function ($scope, serviceDatos, serviceFunciones) {

        $scope.products = serviceDatos.products;
        $scope.carroCompras = serviceDatos.carroCompras;
        $scope.vcarroComprasDef = serviceDatos.vcarroComprasDef;
        $scope.cantidadItem = serviceDatos.cantidadItem;
        $scope.textoAlerta = serviceDatos.textoAlerta;

        if (Object.keys($scope.vcarroComprasDef).length > 0) {
            $scope.carroCompras = serviceFunciones.fnGetCarroCompra($scope.vcarroComprasDef);
        }




        $scope.filtro = {
            'sublevel_id': "",
            'name': "",
            'quantity': "",
            'available': "",
            'price': ""
        };

        $scope.ordernar = "";
        $scope.cantidadItem = 0;
        $scope.textoAlerta = 0;


        StringCategories = JSON.stringify(categories);
        StringCategories = StringCategories.replace(new RegExp('"name"', "gm"), '"title"').replace(new RegExp('"sublevels"', "gm"), '"children"').replace(new RegExp('"id"', "gm"), '"key"');
        categories = JSON.parse(StringCategories);
        $("#MenuTienda").fancytree({

            icon: function (event, data) {
               
                    return "oi oi-aperture colorAzul";
               
                // Otherwise no value is returned, so continue with default processing
            },

            // checkbox: "radio",
            selectMode: 1,
            // tooltip: true,
            // tooltip: function(event, data) { return data.node.title + "!"},
            source: categories.categories
        }).on("click", function (event) {
            Nodo = $.ui.fancytree.getNode(event);
            if (Nodo.children == null) {

                $scope.filtro.sublevel_id = Nodo.key;
                $(".filtroNodo").show(500);
                $(".labelfiltronodo").html("Filtar por " + Nodo.title);


            } else {

                $scope.filtro.sublevel_id = "";
                $(".filtroNodo").hide(500);
                $(".textofiltronodo").val("");

            }

        });

        $scope.fnActualizarBotonItem = function () {
            $scope.cantidadItem = 0;
            $scope.textoAlerta = 0;
            for (var propertyName in $scope.carroCompras) {
                $scope.cantidadItem += $scope.carroCompras[propertyName]["pedido"];
                $scope.textoAlerta += $scope.carroCompras[propertyName]["pedido"] * $scope.carroCompras[propertyName]["onlyPrice"];
            }
            $scope.textoAlerta = "$" + $scope.textoAlerta;
        };


        $scope.fnMAPedido = function () {

            angular.forEach($scope.products, function (value, key) {
                $scope.products[key].pedido = 0;
                $scope.products[key].idProductoJson = key;                
                $scope.products[key].onlyPrice = parseInt($scope.products[key].price.replace(/^\D+/g, '').replace(',', ''));
            });


        }


        $scope.fnActualizarPedido = function (index, accion) {
           

            $scope.products[index].pedido = parseInt($scope.products[index].pedido);

            if (isNaN($scope.products[index].pedido)) {
                $scope.products[index].pedido = 0;
            }


            if ($scope.products[index].pedido > $scope.products[index].quantity) {
                $scope.products[index].pedido = $scope.products[index].quantity;
            }



            if ($scope.products[index].pedido == 1 && accion == '-') {
                $scope.products[index].pedido = 0;
            } else {
                if (accion == '-') {
                    $scope.products[index].pedido = $scope.products[index].pedido - 1;
                }
                if (accion == '+') {
                    $scope.products[index].pedido = $scope.products[index].pedido + 1;
                }

            }

            $scope.carroCompras[$scope.products[index].id] = $scope.products[index];
            $scope.carroCompras[$scope.products[index].id]["idProductoJson"] = index;
            if ($scope.products[index].pedido <= 0) {
                $scope.carroCompras.splice($scope.products[index].id, 1);
                // delete $scope.carroCompras[$scope.products[index].id];
            }

            $scope.vcarroComprasDef = [];
            $scope.vcarroComprasDef = serviceFunciones.fnGetCarroCompraDef($scope.carroCompras);
            serviceDatos.vcarroComprasDef = $scope.vcarroComprasDef;

            localStorage.setItem("vcarroComprasDef", JSON.stringify($scope.vcarroComprasDef));
            localStorage.setItem("products", JSON.stringify($scope.products));

            $scope.fnActualizarBotonItem();

            $("#alerta").show(500);
            setTimeout(function () {
                $("#alerta").hide(500);
            }, 2000);


        };



        if (Object.keys($scope.vcarroComprasDef).length == 0) {
            $scope.fnMAPedido();
        } else {
            $scope.fnActualizarBotonItem();
        }


//        $(document).ready(function () {
//            $('.solonumeros').on('keyup', function () {
//                console.log(1);
//                this.value = this.value.replace(/[^0-9]/g, '');
//            });
//        });



    }]);


appAngular.controller('CarroController', ['$scope', 'serviceDatos', 'serviceFunciones', function ($scope,serviceDatos, serviceFunciones) {


        $scope.products = serviceDatos.products;
        $scope.carroCompras = serviceDatos.carroCompras;
        $scope.vcarroComprasDef = serviceDatos.vcarroComprasDef;

//        $scope.carroComprasListar = [];
//        for (var propertyName in $scope.vcarroComprasDef) {
//            $scope.carroComprasListar.push($scope.vcarroComprasDef[propertyName]);
//        }

        $scope.fnActualizarPedido = function (index, accion) {

            $scope.vcarroComprasDef[index].pedido = parseInt($scope.vcarroComprasDef[index].pedido);

            if (isNaN($scope.vcarroComprasDef[index].pedido)) {
                $scope.vcarroComprasDef[index].pedido = 0;
            }


            //si cantidad insertada es mayor            
            if ($scope.vcarroComprasDef[index].pedido > $scope.vcarroComprasDef[index].quantity) {
                $scope.vcarroComprasDef[index].pedido = $scope.vcarroComprasDef[index].quantity;
            }

            if ($scope.vcarroComprasDef[index].pedido == 1 && accion == '-') {
                $scope.products[$scope.vcarroComprasDef[index].idProductoJson].pedido = 0;
                // delete $scope.vcarroComprasDef[index];
                $scope.vcarroComprasDef.splice(index, 1);
            } else {
                if (accion == '-') {
                    $scope.vcarroComprasDef[index].pedido = $scope.vcarroComprasDef[index].pedido - 1;
                }
                if (accion == '+') {
                    $scope.vcarroComprasDef[index].pedido = $scope.vcarroComprasDef[index].pedido + 1;
                }
                $scope.products[$scope.vcarroComprasDef[index].idProductoJson].pedido = $scope.vcarroComprasDef[index].pedido;
            }



            serviceDatos.vcarroComprasDef = $scope.vcarroComprasDef;

            localStorage.setItem("vcarroComprasDef", JSON.stringify($scope.vcarroComprasDef));
            localStorage.setItem("products", JSON.stringify($scope.products));


        };


    }]);













